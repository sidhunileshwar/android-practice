package com.example.user.first_task;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Open_trends_firstTask extends AppCompatActivity
{

    Button btnShow,btnClear;

    TextView txtMess;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_trends_first_task);

        btnShow=(Button) findViewById(R.id.btnClick);

        btnClear=(Button) findViewById(R.id.btnClear);

        txtMess=(TextView) findViewById(R.id.txtMessage);



        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                txtMess.setText("Hai welcome to opentrends......!!!!");

            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                txtMess.setText("");

            }
        });


    }
}
